<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TrabajadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trabajadors')->insert([
            'usuario' => 'cotizante',
            'contrasena' => Hash::make('cotizante'),
            'id_rol' => 1,
            'nombre' => 'Sebastian Cano'
        ]);
        DB::table('trabajadors')->insert([
            'usuario' => 'modista',
            'contrasena' => Hash::make('modista'),
            'id_rol' => 2,
            'nombre' => 'Sebastian Cano'
        ]);
        DB::table('trabajadors')->insert([
            'usuario' => 'instalador',
            'contrasena' => Hash::make('instalador'),
            'id_rol' => 3,
            'nombre' => 'Sebastian Cano'
        ]);
        DB::table('trabajadors')->insert([
            'usuario' => 'admin',
            'contrasena' => Hash::make('admin'),
            'id_rol' => 4,
            'nombre' => 'Sebastian Cano'
        ]);
    }
}
