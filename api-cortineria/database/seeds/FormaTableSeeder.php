<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forma_pagos')->insert([
            'nombre' => 'Contado',
            'descripcion' => 'Pago inmediato'
        ]);
        DB::table('forma_pagos')->insert([
            'nombre' => 'Credito',
            'descripcion' => 'Pago a cuotas'
        ]);
    }
}
