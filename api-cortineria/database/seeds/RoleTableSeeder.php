<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nombre' => 'cotizante',
            'descripcion' => 'Este rol es el encargado de hacer las visitas para realizar la cotización de la cortineria'
        ]);
        DB::table('roles')->insert([
            'nombre' => 'modista',
            'descripcion' => 'Este rol es el encargado de realizar las cortinas'
        ]);
        DB::table('roles')->insert([
            'nombre' => 'instalador',
            'descripcion' => 'Este rol es el encargado de realizar la instalación de las cortinas'
        ]);
        DB::table('roles')->insert([
            'nombre' => 'admin',
            'descripcion' => 'Este rol es el encargado de crear trabajadores y visualizar gráfica'
        ]);
    }
}
