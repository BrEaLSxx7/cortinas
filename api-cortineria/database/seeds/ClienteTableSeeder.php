<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'cedula' => 1112788893,
            'nombre_completo' => 'Sebastian Cano Cadavid',
            'direccion' => 'Calle 4 # 27-33',
            'telefono' => 3113215870,
            'ciudad' => 'Cartago',
            'barrio' => 'El cipres'
        ]);
    }
}
