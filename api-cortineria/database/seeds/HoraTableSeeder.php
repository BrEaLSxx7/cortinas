<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HoraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('horas')->insert([
            'hora' => '8: 00 AM'
        ]);
        DB::table('horas')->insert([
            'hora' => '9: 00 AM'
        ]);
        DB::table('horas')->insert([
            'hora' => '10: 00 AM'
        ]);
        DB::table('horas')->insert([
            'hora' => '11: 00 AM'
        ]);
        DB::table('horas')->insert([
            'hora' => '12: 00 PM'
        ]);
        DB::table('horas')->insert([
            'hora' => '2: 00 PM'
        ]);
        DB::table('horas')->insert([
            'hora' => '3: 00 PM'
        ]);
        DB::table('horas')->insert([
            'hora' => '4: 00 PM'
        ]);
        DB::table('horas')->insert([
            'hora' => '5: 00 PM'
        ]);
    }
}
