<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HoraTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(TrabajadorTableSeeder::class);
        $this->call(ClienteTableSeeder::class);
        $this->call(FormaTableSeeder::class);
    }
}
