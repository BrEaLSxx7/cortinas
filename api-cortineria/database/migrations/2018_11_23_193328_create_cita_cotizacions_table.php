<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitaCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cita_cotizacions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->boolean('aprobado')->nullable();
            $table->integer('id_cliente')->unsigned();
            $table->integer('id_hora')->unsigned()->nullable();
            $table->string('tipo_trabajo', 25);
            $table->boolean('output')->default(false);
            $table->timestamps();

            $table->foreign('id_cliente')->references('id')->on('clientes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_hora')->references('id')->on('horas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cita_cotizacions');
    }
}
