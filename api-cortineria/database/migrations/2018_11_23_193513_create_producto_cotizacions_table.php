<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_cotizacions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->float('ancho')->unsigned();
            $table->float('largo')->unsigned();
            $table->float('valor_unitario')->unsigned();
            $table->string('color', 25);
            $table->string('referencia', 25);
            $table->string('ubicacion', 50);
            $table->integer('cantidad')->unsigned();
            $table->float('precio_total')->unsigned();
            $table->float('mts_total')->unsigned();
            $table->boolean('terminado')->default(false);
            $table->boolean('instalado')->default(false);
            $table->boolean('pertenece_fab')->default(false);
            $table->integer('id_cotizacion')->unsigned();
            $table->timestamps();


            $table->foreign('id_cotizacion')->references('id')->on('cotizacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_cotizacions');
    }
}
