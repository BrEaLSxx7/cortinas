<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizacions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_instalacion_men');
            $table->date('fecha_instalacion_may');
            $table->integer('id_cita')->unsigned();
            $table->integer('id_forma')->unsigned();
            $table->boolean('aprobado')->nullable();
            $table->timestamps();

            $table->foreign('id_cita')->references('id')->on('cita_cotizacions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_forma')->references('id')->on('forma_pagos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizacions');
    }
}
