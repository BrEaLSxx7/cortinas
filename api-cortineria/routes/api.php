<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('client', 'ClienteController');
Route::apiResource('cita', 'CitaCotizacionController');
Route::apiResource('cotizacion', 'CotizacionController');
Route::apiResource('product', 'ProductoCotizacionController');
Route::apiResource('trabajador', 'TrabajadorController');
Route::apiResource('rol', 'RoleController');
Route::get('search', 'ClienteController@search');
Route::get('hour', 'HoraController@habilitadas');
Route::get('instalar', 'CotizacionController@instalar');
Route::get('consultas', 'ClienteController@consultar');
Route::get('nofab', 'ProductoCotizacionController@nofab');
Route::get('data', 'ProductoCotizacionController@data');
Route::post('auth', 'TrabajadorController@auth');
Route::post('terminar', 'ProductoCotizacionController@terminar');
Route::post('instalado', 'ProductoCotizacionController@instalado');
Route::put('cancel', 'CitaCotizacionController@cancel');
Route::put('aprobar', 'CotizacionController@aprobar');
Route::delete('delete', 'TrabajadorController@delete');
Route::get('apk', function () {
    return response()->download('../../cortineria-app/platforms/android/app/build/outputs/apk/debug/app-debug.apk');
});
