<?php

namespace App\Http\Controllers;

use App\cita_cotizacion;
use App\cotizacion;
use App\forma_pago;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            if (isset($request->fecha)) {
                $cotizacion = DB::table('cotizacions')
                    ->join('cita_cotizacions', 'cotizacions.id_cita', '=', 'cita_cotizacions.id')
                    ->join('forma_pagos', 'cotizacions.id_forma', '=', 'forma_pagos.id')
                    ->join('clientes', 'cita_cotizacions.id_cliente', '=', 'clientes.id')
                    ->select('clientes.cedula', 'cotizacions.id', 'clientes.ciudad', 'clientes.nombre_completo as nombre', 'clientes.telefono', 'cotizacions.fecha_instalacion_men as fecha', 'cotizacions.aprobado', 'forma_pagos.nombre as forma')
                    ->orderBy('cotizacions.aprobado', 'desc')
                    ->get();
                return response()->json($cotizacion, 200);
            }
            $cotizacion = DB::table('producto_cotizacions')
                ->join('cotizacions', 'cotizacions.id', '=', 'producto_cotizacions.id_cotizacion')
                ->join('cita_cotizacions', 'cita_cotizacions.id', '=', 'cotizacions.id_cita')
                ->join('clientes', 'cita_cotizacions.id_cliente', '=', 'clientes.id')
                ->select('clientes.cedula', 'clientes.ciudad', 'producto_cotizacions.id', 'cotizacions.fecha_instalacion_men as fecha', 'clientes.nombre_completo as nombre', 'producto_cotizacions.descripcion', 'producto_cotizacions.ancho', 'producto_cotizacions.largo', 'producto_cotizacions.cantidad', 'producto_cotizacions.color', 'producto_cotizacions.mts_total as mts')
                ->where('cotizacions.aprobado', true)
                ->where('producto_cotizacions.terminado', false)
                ->where('producto_cotizacions.pertenece_fab', true)
                ->orderBy('cotizacions.fecha_instalacion_men', 'asc')
                ->get();
            return response()->json($cotizacion, 200);
        }
        return response()->json(['message' => 'Por favor authenticarse'], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isJson()) {
            $request->validate([
                'forma' => 'required|string',
                'fecha' => 'required|string'
            ], [
                'required' => 'El campo :attribute es requerido',
                'string' => 'El campo :attribute deberia tener un formato valido'
            ]);
            try {
                $forma = forma_pago::where('nombre', $request->forma)->firstOrFail();
                $cita = cita_cotizacion::where('id', $request->id)->firstOrFail();
                $fecha = new Carbon($request->fecha);
                $fecha2 = new Carbon($request->fecha);
                $cotizacion = new cotizacion();
                $men = 9;
                $max = 11;
                $men = $fecha->setDate($fecha->year, $fecha->month, $fecha->day + $men);
                $max = $fecha2->setDate($fecha2->year, $fecha2->month, $fecha2->day + $max);
                $cotizacion->fecha_instalacion_men = $men;
                $cotizacion->fecha_instalacion_may = $max;
                $cotizacion->id_forma = $forma->id;
                $cotizacion->id_cita = $request->id;
                $cita->aprobado = true;
                if ($request->forma === 'Contado') {
                    $cotizacion->aprobado = true;
                }
                $cita->saveOrFail();
                $cotizacion->saveOrFail();
                return response()->json($cotizacion, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function show(cotizacion $cotizacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function edit(cotizacion $cotizacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cotizacion $cotizacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cotizacion $cotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(cotizacion $cotizacion)
    {
        //
    }

    public function instalar(Request $request)
    {
        if ($request->isJson()) {
            $cotizacion = DB::table('producto_cotizacions')
                ->join('cotizacions', 'cotizacions.id', '=', 'producto_cotizacions.id_cotizacion')
                ->join('cita_cotizacions', 'cita_cotizacions.id', '=', 'cotizacions.id_cita')
                ->join('clientes', 'cita_cotizacions.id_cliente', '=', 'clientes.id')
                ->select('clientes.cedula', 'clientes.ciudad', 'producto_cotizacions.id', 'cotizacions.fecha_instalacion_may as fecha_may', 'cotizacions.fecha_instalacion_men as fecha_men', 'clientes.nombre_completo as nombre', 'producto_cotizacions.descripcion', 'clientes.direccion', 'clientes.barrio', 'telefono')
                ->where('cotizacions.aprobado', true)
                ->where('producto_cotizacions.terminado', true)
                ->where('producto_cotizacions.instalado', false)
                ->orderBy('cotizacions.fecha_instalacion_may', 'asc')
                ->get();
            return response()->json($cotizacion);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function aprobar(Request $request)
    {
        if ($request->isJson()) {
            try {
                $cotizacion = cotizacion::where('id', $request->id)->firstOrFail();
                if ($request->aprobado === 'null') {
                    $forma = forma_pago::where('nombre', 'Contado')->firstOrFail();
                    $cotizacion->id_forma = $forma->id;
                    $cotizacion->aprobado = true;
                } else {
                    $cotizacion->aprobado = $request->aprobado;
                }
                $msm = $cotizacion->aprobado ? 'Aprobado correctamente' : 'No aprobado correctamente';
                $cotizacion->saveOrFail();
                return response()->json(['message' => $msm], 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json($exception->getMessage(), 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
