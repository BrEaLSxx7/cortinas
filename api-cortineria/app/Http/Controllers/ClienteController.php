<?php

namespace App\Http\Controllers;

use App\cita_cotizacion;
use App\cliente;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $c = cliente::all();
        return response()->json($c);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isJson()) {
            $request->validate([
                'barrio' => 'required|string|max:25',
                'cedula' => 'required|integer',
                'telefono' => 'required',
                'direccion' => 'required|string|max:50',
                'nombre_completo' => 'required|string|max:100',
                'ciudad' => 'required|string|max:50',
            ], [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute deberia tener un formato valido',
                'max' => 'El campo :attribute deberia tener menos caracteres',
                'string' => 'El campo :attribute deberia tener un formato valido'
            ]);
            try {
                $cliente = new cliente();
                $cliente->fill($request->all());
                $cliente->saveOrFail();
                return response()->json(['message' => 'Cliente creado correctamente', 'cliente' => $cliente], 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public
    function show(cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public
    function edit(cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cliente $cliente)
    {
        if ($request->isJson()) {
            $request->validate([
                'barrio' => 'required|string|max:25',
                'cedula' => 'required|integer',
                'telefono' => 'required',
                'direccion' => 'required|string|max:50',
                'ciudad' => 'required|string|max:50',
                'nombre_completo' => 'required|string|max:100',
            ], [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute deberia tener un formato valido',
                'max' => 'El campo :attribute deberia tener menos caracteres',
                'string' => 'El campo :attribute deberia tener un formato valido'
            ]);
            try {
                $cliente = cliente::where('id', $request->id)->first();
                $cliente->fill($request->all());
                $cliente->saveOrFail();
                return response()->json(['message' => 'Cliente Actualizado correctamente', 'cliente' => $cliente], 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(cliente $cliente)
    {
        //
    }

    public function search(Request $request)
    {

        if ($request->isJson()) {
            $request->validate([
                'cc' => 'required|integer',
            ], [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute deberia tener un formato valido'
            ]);
            try {
                $cliente = cliente::where('cedula', $request->cc)->firstOrFail();
                return response()->json($cliente, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => 'No se encontro el cliente ¿desea crearlo?'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function consultar(Request $request)
    {
        if ($request->isJson()) {
            try {
                $cliente = cliente::where('cedula', $request->cc)->firstOrFail();
                $citas = DB::table('cita_cotizacions')
                    ->join('horas', 'horas.id', 'cita_cotizacions.id_hora')
                    ->select('cita_cotizacions.id', 'cita_cotizacions.fecha', 'horas.hora', 'cita_cotizacions.tipo_trabajo', 'cita_cotizacions.aprobado')
                    ->where('cita_cotizacions.id_cliente', $cliente->id)
                    ->get();
                $cliente->citas = $citas;
                $cotizaciones = DB::table('producto_cotizacions')
                    ->join('cotizacions', 'cotizacions.id', '=', 'producto_cotizacions.id_cotizacion')
                    ->join('cita_cotizacions', 'cita_cotizacions.id', '=', 'cotizacions.id_cita')
                    ->join('clientes', 'clientes.id', '=', 'cita_cotizacions.id_cliente')
                    ->join('forma_pagos', 'forma_pagos.id', '=', 'cotizacions.id_forma')
                    ->select('producto_cotizacions.descripcion', 'producto_cotizacions.terminado', 'cotizacions.aprobado', 'forma_pagos.nombre as forma', 'producto_cotizacions.id')
                    ->where('clientes.id', $cliente->id)
                    ->get();
                $cliente->cotizaciones = $cotizaciones;
                return response()->json($cliente, 200);
            } catch
            (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
