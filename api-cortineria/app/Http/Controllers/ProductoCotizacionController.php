<?php

namespace App\Http\Controllers;

use App\cita_cotizacion;
use App\cotizacion;
use App\producto_cotizacion;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductoCotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isJson()) {
            $request->validate([
                'descripcion' => 'required|string|max:200',
                'ancho' => 'required|between:0,9999.99',
                'largo' => 'required|between:0,9999.99',
                'valor_unitario' => 'required|between:0,9999.99',
                'color' => 'required|string|max:25',
                'referencia' => 'required|string|max:25',
                'ubicacion' => 'required|string|max:50',
                'cantidad' => 'required|integer',
                'id_cotizacion' => 'required|integer',
                'pertenece_fab' => 'required|boolean'
            ], [
                'required' => 'El campo :attribute es requerido',
                'string' => 'El campo :attribute deberia tener un formato valido',
                'max' => 'El campo :attribute deberia tener menos caracteres',
                'between' => 'El campo :attribute deberia tener un formato valido',
                'integer' => 'El campo :attribute deberia tener un formato valido',
                'boolean' => 'El campo :attribute deberia tener un formato valido'
            ]);

            try {
                $producto_cotizacion = new producto_cotizacion();
                $producto_cotizacion->fill($request->all());
                $producto_cotizacion->precio_total = $request->cantidad * $request->valor_unitario;
                $producto_cotizacion->mts_total = $request->ancho * $request->largo;
                $producto_cotizacion->saveOrFail();
                return response()->json(['message' => 'Producto agregado correctamente'], 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\producto_cotizacion $producto_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function show(producto_cotizacion $producto_cotizacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\producto_cotizacion $producto_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function edit(producto_cotizacion $producto_cotizacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\producto_cotizacion $producto_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, producto_cotizacion $producto_cotizacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\producto_cotizacion $producto_cotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(producto_cotizacion $producto_cotizacion)
    {
        //
    }

    public function terminar(Request $request)
    {
        if ($request->isJson()) {
            $con = 0;
            $ele = [];
            try {
                foreach ($request->all() as $item) {
                    $producto = producto_cotizacion::where('id', $item['id'])->firstOrFail();
                    $producto->terminado = true;
                    $tmp = $producto->save();
                    $ele[] = !$tmp ? $producto->id : [];
                    $con += $tmp ? 0 : 1;
                }
                if ($con === 0) {
                    return response()->json(['message' => 'Productos terminados correctamente'], 200);
                }
                return response()->json(['errors' => $ele], 406);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function instalado(Request $request)
    {
        if ($request->isJson()) {
            $con = 0;
            $ele = [];
            try {
                foreach ($request->all() as $item) {
                    $producto = producto_cotizacion::where('id', $item['id'])->firstOrFail();
                    $producto->instalado = true;
                    $tmp = $producto->save();
                    $ele[] = !$tmp ? $producto->id : [];
                    $con += $tmp ? 0 : 1;
                }
                if ($con === 0) {
                    return response()->json(['message' => 'Productos terminados correctamente'], 200);
                }
                return response()->json(['errors' => $ele], 406);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function nofab(Request $request)
    {
        if ($request->isJson()) {
            try {
                $cotizacion = DB::table('producto_cotizacions')
                    ->join('cotizacions', 'cotizacions.id', '=', 'producto_cotizacions.id_cotizacion')
                    ->join('cita_cotizacions', 'cita_cotizacions.id', '=', 'cotizacions.id_cita')
                    ->join('clientes', 'cita_cotizacions.id_cliente', '=', 'clientes.id')
                    ->select('clientes.cedula', 'producto_cotizacions.id', 'clientes.ciudad', 'cotizacions.fecha_instalacion_men as fecha', 'clientes.nombre_completo as nombre', 'producto_cotizacions.descripcion', 'producto_cotizacions.ancho', 'producto_cotizacions.largo', 'producto_cotizacions.cantidad', 'producto_cotizacions.color', 'producto_cotizacions.mts_total as mts')
                    ->where('cotizacions.aprobado', true)
                    ->where('producto_cotizacions.terminado', false)
                    ->where('producto_cotizacions.pertenece_fab', false)
                    ->orderBy('cotizacions.fecha_instalacion_men', 'asc')
                    ->get();
                return response()->json($cotizacion, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function data(Request $request)
    {
        if ($request->isJson()) {
            try {
                $totalCitas = 0;
                $citas = [];
                $totalCotizaciones = 0;
                $cotizaciones = [];
                $totalAprobadas = 0;
                $aprobadas = [];
                $totalNoAprobadas = 0;
                $noAprobadas = [];
                for ($i = 1; $i <= 12; $i++) {
                    $tmp = cita_cotizacion::whereYear('fecha', $request->ano)->whereMonth('fecha', $i)->get()->count();
                    $tmp2 = cotizacion::whereYear('created_at', $request->ano)->whereMonth('created_at', $i)->get()->count();
                    $tmp3 = cotizacion::whereYear('created_at', $request->ano)->whereMonth('created_at', $i)->where('aprobado', true)->get()->count();
                    $tmp4 = cotizacion::whereYear('created_at', $request->ano)->whereMonth('created_at', $i)->where('aprobado', false)->get()->count();
                    $tmp5 = cita_cotizacion::whereYear('fecha', $request->ano)->whereMonth('fecha', $i)->where('aprobado', false)->get()->count();
                    $totalCitas += $tmp;
                    $citas[] = $tmp;
                    $totalCotizaciones += $tmp2;
                    $cotizaciones[] = $tmp2;
                    $totalAprobadas += $tmp3;
                    $aprobadas[] = $tmp3;
                    $totalNoAprobadas += $tmp4 + $tmp5;
                    $noAprobadas[] = $tmp4 + $tmp5;
                }
                return response()->json([
                    'totalCitas' => $totalCitas, 'citas' => $citas,
                    'totalCotizaciones' => $totalCotizaciones, 'cotizaciones' => $cotizaciones,
                    'totalAprobadas' => $totalAprobadas, 'aprobadas' => $aprobadas,
                    'totalNoAprobados' => $totalNoAprobadas, 'noAprobados' => $noAprobadas
                ], 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
