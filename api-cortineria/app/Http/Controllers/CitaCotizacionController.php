<?php

namespace App\Http\Controllers;

use App\cita_cotizacion;
use App\cliente;
use App\hora;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CitaCotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            try {
                $fecha = new Carbon($request->fecha);
                $citas = cita_cotizacion::whereDate('fecha', $fecha)->where('aprobado', null)->orderBy('id_hora', 'asc')->get();
                foreach ($citas as $ind => $ele) {
                    $hora = hora::where('id', $ele->id_hora)->firstOrFail();
                    $cliente = cliente::where('id', $ele->id_cliente)->firstOrFail();
                    $citas[$ind]['hora_cita'] = $hora;
                    $citas[$ind]['cliente_cita'] = $cliente;
                }
                $result = $this->unique($citas, 'id_cliente');
                return response()->json($result, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    private function unique($array, string $key)
    {
        $temp_array = [];
        foreach ($array as &$v) {
            if (!isset($temp_array[$v[$key]]))
                $temp_array[$v[$key]] =& $v;
        }
        $array = array_values($temp_array);
        return $array;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        if ($request->isJson()) {
            $request->validate([
                'fecha' => 'required|date',
                'tipo_trabajo' => 'required|string|max:25',
                'id_cliente' => 'required|integer',
                'id_hora' => 'required',
            ], [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute deberia tener un formato valido',
                'date' => 'El campo :attribute deberia tener un formato valido',
                'string' => 'El campo :attribute deberia tener un formato valido',
                'bolean' => 'El campo :attribute deberia tener un formato valido'
            ]);
            try {
                $cita = new cita_cotizacion();
                $cita->fill($request->all());
                $cita->fecha = new Carbon($request->fecha);
                $cita->id_hora = $request->id_hora;
                $cita->output = $request->output;
                $cita->saveOrFail();
                return response()->json(['message' => 'La cita fue reservada correctamente', 'cita' => $cita], 201);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cita_cotizacion $cita_cotizacion
     * @return \Illuminate\Http\Response
     */
    public
    function show(cita_cotizacion $cita_cotizacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cita_cotizacion $cita_cotizacion
     * @return \Illuminate\Http\Response
     */
    public
    function edit(cita_cotizacion $cita_cotizacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\cita_cotizacion $cita_cotizacion
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, cita_cotizacion $cita_cotizacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cita_cotizacion $cita_cotizacion
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(cita_cotizacion $cita_cotizacion)
    {
        //
    }

    public
    function cancel(Request $request)
    {
        if ($request->isJson()) {
            try {
                $cita = cita_cotizacion::where('id', $request->id)->firstOrFail();
                $cita->aprobado = false;
                $cita->saveOrFail();
                $fecha = new Carbon($request->fecha);
                $citas = cita_cotizacion::whereDate('fecha', $fecha)->where('aprobado', null)->orderBy('id_hora', 'asc')->get();
                foreach ($citas as $ind => $ele) {
                    $hora = hora::where('id', $ele->id_hora)->firstOrFail();
                    $cliente = cliente::where('id', $ele->id_cliente)->firstOrFail();
                    $citas[$ind]['hora_cita'] = $hora;
                    $citas[$ind]['cliente_cita'] = $cliente;
                }
                return response()->json(['citas' => $citas], 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor authenticarse'], 401);
    }
}
