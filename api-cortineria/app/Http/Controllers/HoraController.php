<?php

namespace App\Http\Controllers;

use App\cita_cotizacion;
use App\hora;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\hora $hora
     * @return \Illuminate\Http\Response
     */
    public function show(hora $hora)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hora $hora
     * @return \Illuminate\Http\Response
     */
    public function edit(hora $hora)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\hora $hora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, hora $hora)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\hora $hora
     * @return \Illuminate\Http\Response
     */
    public function destroy(hora $hora)
    {
        //
    }

    public function habilitadas(Request $request)
    {
        $output = [];
        if ($request->isJson()) {
            $fecha = new Carbon($request->fecha);
            $festivos = new festivos();
            $festivos->festivos($fecha->year);
            if (isset($festivos->festivos[$fecha->year][$fecha->month][$fecha->day])) {
                return response()->json(['message' => 'El dia es festivo por favor selecione otro'], 500);
            }
            $citas = cita_cotizacion::whereDate('fecha', $fecha)->get();
            $horas = hora::all();
            foreach ($horas as $index => $element) {
                foreach ($citas as $element2) {
                    if ($element->id === (int)$element2->id_hora) {
                        unset($horas[$index]);
                    }
                }
            }
            foreach ($citas as $item) {
                if ($item->output) {
                    $output[] = $item->id_hora;
                }
            }
            switch (count($output)) {
                case 2:
                    return response()->json(['message' => 'Por favor selecione otra fecha, ya se encuentra la agenda copada'], 401);
                case 1:
                    $tmp = $this->filterOutput($horas, $output);
                    break;
                default:
                    $tmp = [];
                    foreach ($horas as $ele) {
                        $tmp[] = $ele;
                    }
                    if (count($tmp) < 1) {
                        return response()->json(['message' => 'Por favor selecione otra fecha, ya se encuentra la agenda copada'], 401);
                    }
                    break;
            }
            return response()->json($tmp, 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    private function filterOutput($data, array $id)
    {
        $horas = [];
        foreach ($id as $ele) {
            foreach ($data as $ind => $item) {
                switch ($ele) {
                    case 1:
                        if ($item->id <= 5) {
                            unset($data[$ind]);
                        }
                        break;
                    case 6:
                        if ($item->id >= 6) {
                            unset($data[$ind]);
                        }
                        break;
                    default:
                        return ['message' => 'Lo sentimos ocurrió un error'];
                }
            }
        }
        foreach ($data as $item) {
            $horas[] = $item;
        }
        return $horas;
    }
}
