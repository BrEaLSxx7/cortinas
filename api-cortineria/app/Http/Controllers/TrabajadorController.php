<?php

namespace App\Http\Controllers;


use App\role;
use App\trabajador;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TrabajadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson()) {
            try {
                $trabajador = trabajador::all()->where('id_rol', '<>', 4);
                foreach ($trabajador as $item) {
                    $item->rol_trabajador;
                    $item->password = '*************';
                }
                $data = [];
                foreach ($trabajador as $item) {
                    $data[] = $item;
                }
                return response()->json($data, 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $trabajador = new trabajador();
            $trabajador->fill($request->all());
            $trabajador->contrasena = Hash::make($request->contrasena);
            $trabajador->saveOrFail();
            return response()->json(['message' => 'Trabajador creado correctamente'], 201);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\trabajador $trabajador
     * @return \Illuminate\Http\Response
     */
    public function show(trabajador $trabajador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\trabajador $trabajador
     * @return \Illuminate\Http\Response
     */
    public function edit(trabajador $trabajador)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\trabajador $trabajador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, trabajador $trabajador)
    {
        if ($request->isJson()) {
            try {
                $trabajador->fill($request->all());
                if ($request->contrasena !== '*************') {
                    $trabajador->contrasena = Hash::make($request->contrasena);
                }
                $trabajador->saveOrFail();
                return response()->json(['message' => 'Trabajador actualizado correctamente'], 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\trabajador $trabajador
     * @return \Illuminate\Http\Response
     */
    public function destroy(trabajador $trabajador)
    {
        //
    }

    public function auth(Request $request)
    {
        if ($request->isJson()) {
            $request->validate([
                'usuario' => 'required|string|max:50',
                'contrasena' => 'required|string|max:50'
            ], [
                'required' => 'El campo :attribute es requerido',
                'string' => 'El campo :attribute deberia tener un formato valido',
                'max' => 'El campo :attribute no deberia tener mas de 50 caracteres'
            ]);
            try {
                $request->usuario = base64_decode($request->usuario);
                $request->contrasena = base64_decode($request->contrasena);
                $auth = trabajador::where('usuario', $request->usuario)->firstOrFail();
                if (Hash::check($request->contrasena, $auth->contrasena)) {
                    $auth->rol_trabajador;
                    return response()->json(['user' => $auth], 200);
                }
                return response()->json(['message' => 'Correo y/o contraseña incorrectos'], 401);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => 'Correo y/o contraseña incorrectos'], 401);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function delete(Request $request)
    {
        if ($request->isJson()) {
            try {
                $trabajador = trabajador::where('id', $request->id)->firstOrFail();
                $trabajador->delete();
                return response()->json(['message' => 'Borrado exitosamente'], 200);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

//    public function getTrabajador(Request $request)
//    {
//        if ($request->isJson()) {
//            try {
//                return response()->json(trabajador::where('id', $request->id)->firstOrFail(), 200);
//            } catch (ModelNotFoundException $exception) {
//                return response()->json(['message' => $exception->getMessage()], 500);
//            }
//        }
//        return response()->json(['message' => 'Por favor autenticarse'], 401);
//    }
}
