<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cita_cotizacion extends Model
{
    protected $fillable = ['id_cliente', 'tipo_trabajo'];
    protected $hidden = ['created_at', 'updated_at'];

    public function cliente_cita()
    {
        return $this->belongsTo(cliente::class, 'id_cliente');
    }
    public function hora_cita()
    {
        return $this->belongsTo(hora::class, 'id_cliente');
    }
}
