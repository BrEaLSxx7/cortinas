<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto_cotizacion extends Model
{
    protected $fillable = ["descripcion", "ancho", "largo", "valor_unitario", "color", "referencia", "ubicacion", "cantidad", 'id_cotizacion', 'pertenece_fab'];
    protected $hidden = ['created_at', 'updated_at'];
}
