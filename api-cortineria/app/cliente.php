<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $fillable = ['barrio', 'cedula', 'direccion', 'nombre_completo', 'telefono', 'ciudad'];
    protected $hidden = ['created_at', 'updated_at'];
}
