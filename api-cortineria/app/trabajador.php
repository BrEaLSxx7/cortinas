<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trabajador extends Model
{
    protected $fillable = ['id_rol', 'nombre', 'usuario'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'contrasena', 'id_rol'];

    public function rol_trabajador()
    {
        return $this->belongsTo(role::class, 'id_rol');
    }
}
