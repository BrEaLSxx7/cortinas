<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hora extends Model
{
    protected $fillable = ['hora'];
    protected $hidden = ['created_at', 'updated_at'];
}
