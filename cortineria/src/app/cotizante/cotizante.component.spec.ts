import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizanteComponent } from './cotizante.component';

describe('CotizanteComponent', () => {
  let component: CotizanteComponent;
  let fixture: ComponentFixture<CotizanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
