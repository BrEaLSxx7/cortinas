import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {Cotizaciones} from '../interfaces/modista/cotizaciones';
import {ToastrService} from 'ngx-toastr';
import {InstaladorService} from '../services/instalador/instalador.service';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-instalador',
  templateUrl: './instalador.component.html',
  styleUrls: ['./instalador.component.scss']
})
export class InstaladorComponent implements OnInit {
  public user;
  public displayedColumns: string[] = ['id', 'nombre', 'descripcion', 'telefono', 'direccion', 'barrio', 'ciudad', 'entre', 'select'];
  public displayedColumns2: string[] = ['id', 'nombre', 'descripcion', 'telefono', 'direccion', 'barrio', 'ciudad', 'entre', 'select'];
  public dataSource;
  public dataSource2;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  public selection = new SelectionModel<Cotizaciones>(true, []);
  public spinner = true;

  constructor(private _router: Router, private _toastr: ToastrService, private _instalador: InstaladorService) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('qwertylkjh') === null || JSON.parse(sessionStorage.getItem('qwertylkjh')).user.usuario !== 'instalador') {
      this._router.navigate(['']);
    }
    this.user = JSON.parse(sessionStorage.getItem('qwertylkjh'));
    this.cotizacion();
  }

  public logout() {
    sessionStorage.clear();
    this._router.navigate(['']);
    this._toastr.success('Sesión cerrada');
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  public cotizacion() {
    this.spinner = true;
    this._instalador.getInstalaciones().subscribe(response => {
      response.forEach(e => {
        e.entre = e.fecha_men + ' / ' + e.fecha_may;
      });
      this.dataSource = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource2 = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource2.paginator = this.paginator2;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public terminar() {
    this.spinner = true;
    if (this.selection.selected.length > 0) {
      this._instalador.instalado(this.selection.selected).subscribe(response => {
        this._toastr.success(response.message);
        this.cotizacion();
      }, error1 => {
        console.error(error1);
        this._toastr.error(error1.error.message);
        this.spinner = false;
      });
    } else {
      this._toastr.error('Por favor selecione una instalación');
      this.spinner = false;
    }
  }
}
