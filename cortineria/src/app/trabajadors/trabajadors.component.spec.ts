import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrabajadorsComponent } from './trabajadors.component';

describe('TrabajadorsComponent', () => {
  let component: TrabajadorsComponent;
  let fixture: ComponentFixture<TrabajadorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrabajadorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrabajadorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
