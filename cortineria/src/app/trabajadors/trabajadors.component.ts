import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Trabajadores} from '../interfaces/admin/trabajadores';
import {AdminServiceService} from '../services/admin/admin-service.service';
import {Rol} from '../interfaces/global/rol';
import {ToastrService} from 'ngx-toastr';
import {Message} from '../interfaces/global/message';
import {SocketService} from '../services/notifications/socket.service';

@Component({
  selector: 'app-trabajadors',
  templateUrl: './trabajadors.component.html',
  styleUrls: ['./trabajadors.component.scss']
})
export class TrabajadorsComponent implements OnInit {
  public type: string;
  public form: FormGroup;
  public trabajador: Trabajadores;
  public roles: Rol[];
  public spinner = true;

  constructor(private _AdminService: AdminServiceService, private _toastr: ToastrService, private _socket: SocketService) {
    this.form = new FormGroup({
      nombre: new FormControl('', Validators.required),
      usuario: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required),
      id_rol: new FormControl('', Validators.required)
    })
    ;
  }

  ngOnInit() {
    if (JSON.parse(sessionStorage.getItem('tbjd')).type === 'Editar') {
      this.trabajador = JSON.parse(sessionStorage.getItem('tbjd'));
      this.form.controls['nombre'].setValue(this.trabajador.nombre);
      this.form.controls['usuario'].setValue(this.trabajador.usuario);
      this.form.controls['contrasena'].setValue(this.trabajador.password);
      this.form.controls['id_rol'].setValue(this.trabajador.rol_trabajador.id);
    }
    this.type = JSON.parse(sessionStorage.getItem('tbjd')).type;
    this._AdminService.getRol().subscribe((response: Rol[]) => {
      this.roles = response;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public actgua(form: FormGroup) {
    this.spinner = true;
    switch (this.type) {
      case 'Editar':
        form.value.id = this.trabajador.id;
        this._AdminService.updateTrabajador(form.value).subscribe((response: Message) => {
          this._toastr.success(response.message);
          this.spinner = false;
          this._socket.sendMessage('Hola');
        }, error1 => {
          console.error(error1);
          this._toastr.error(error1.error.message);
          this.spinner = false;
        });
        break;
      case 'Crear':
        this._AdminService.newTrabajador(form.value).subscribe((response: Message) => {
          this._toastr.success(response.message);
          this.spinner = false;
          this._socket.sendMessage('Hola');
        }, error1 => {
          console.error(error1);
          this._toastr.error(error1.error.message);
          this.spinner = false;
        });
        break;
      default:
        this._toastr.error('Unauthorized');
        this.spinner = false;
        break;
    }
  }
}
