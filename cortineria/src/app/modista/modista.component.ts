import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {ModistaService} from '../services/modista/modista.service';
import {SelectionModel} from '@angular/cdk/collections';
import {Cotizaciones} from '../interfaces/modista/cotizaciones';

@Component({
  selector: 'app-modista',
  templateUrl: './modista.component.html',
  styleUrls: ['./modista.component.scss']
})
export class ModistaComponent implements OnInit {
  public user;
  public displayedColumns: string[] = ['id', 'nombre', 'cantidad', 'color', 'descripcion', 'ancho', 'largo', 'mts', 'fecha', 'select'];
  public displayedColumns2: string[] = ['id', 'nombre', 'cantidad', 'color', 'descripcion', 'ancho', 'largo', 'mts', 'fecha', 'select'];
  public dataSource;
  public dataSource2;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  public selection = new SelectionModel<Cotizaciones>(true, []);
  public fecha = new Date();
  public spinner = true;


  constructor(private _router: Router, private _toastr: ToastrService, private _modistaService: ModistaService) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('qwertylkjh') === null || JSON.parse(sessionStorage.getItem('qwertylkjh')).user.usuario !== 'modista') {
      this._router.navigate(['']);
    }
    this.user = JSON.parse(sessionStorage.getItem('qwertylkjh'));
    this._modistaService.getCotizaciones().subscribe((response: Cotizaciones[]) => {
      response.forEach(e => {
        e.ancho += ' mts';
        e.largo += ' mts';
        e.mts += ' mts';
      });
      this.dataSource = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource2 = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource2.paginator = this.paginator2;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public logout() {
    sessionStorage.clear();
    this._router.navigate(['']);
    this._toastr.success('Sesión cerrada');
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  public cotizacion() {
    this.spinner = true;
    this._modistaService.getCotizaciones().subscribe(response => {
      response.forEach(e => {
        e.ancho += ' mts';
        e.largo += ' mts';
        e.mts += ' mts';
      });
      this.dataSource = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource2 = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource2.paginator = this.paginator2;
      this.spinner = false;
    }, error1 => {
      console.error(error1.error.message);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public terminar() {
    this.spinner = true;
    if (this.selection.selected.length > 0) {
      this._modistaService.terminarProducto(this.selection.selected).subscribe(response => {
        this._toastr.success(response.message);
        this.cotizacion();
      }, error => {
        console.error(error);
        this._toastr.error(error.error.message);
        this.spinner = false;
      });
    } else {
      this._toastr.error('Por favor selecione un producto a terminar');
      this.spinner = false;
    }
  }
}
