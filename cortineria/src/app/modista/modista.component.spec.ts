import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModistaComponent } from './modista.component';

describe('ModistaComponent', () => {
  let component: ModistaComponent;
  let fixture: ComponentFixture<ModistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
