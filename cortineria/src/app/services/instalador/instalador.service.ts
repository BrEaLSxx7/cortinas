import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Cotizaciones} from '../../interfaces/modista/cotizaciones';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Message} from '../../interfaces/global/message';

@Injectable({
  providedIn: 'root'
})
export class InstaladorService {

  constructor(private _http: HttpClient) {
  }

  public getInstalaciones(): Observable<Cotizaciones[]> {
    return this._http.get<Cotizaciones[]>(`${environment.url}instalar`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Cotizaciones[]>) => response.body));
  }

  public instalado(data): Observable<Message> {
    return this._http.post<Message>(`${environment.url}instalado`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }
}
