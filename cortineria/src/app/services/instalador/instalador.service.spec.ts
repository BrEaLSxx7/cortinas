import { TestBed } from '@angular/core/testing';

import { InstaladorService } from './instalador.service';

describe('InstaladorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstaladorService = TestBed.get(InstaladorService);
    expect(service).toBeTruthy();
  });
});
