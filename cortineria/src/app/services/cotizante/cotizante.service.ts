import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Citas} from '../../interfaces/cotizante/citas';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Cotizaciones} from '../../interfaces/cotizante/cotizaciones';
import {AddCotizacion} from '../../interfaces/cotizante/add-cotizacion';
import {CancelCita} from '../../interfaces/cotizante/cancel-cita';
import {Message} from '../../interfaces/global/message';
import {CotizacionesInstalador} from '../../interfaces/instalador/cotizaciones';

@Injectable({
  providedIn: 'root'
})
export class CotizanteService {

  constructor(private _http: HttpClient) {
  }

  public filterFecha(fecha): Observable<Citas[]> {
    return this._http.get<Citas[]>(`${environment.url}cita`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {'fecha': fecha}
    }).pipe(map((response: HttpResponse<Citas[]>) => response.body));
  }

  public addCotizacion(forma): Observable<AddCotizacion> {
    return this._http.post<AddCotizacion>(`${environment.url}cotizacion`, forma, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<AddCotizacion>) => response.body));
  }

  public cancelCita(id): Observable<CancelCita> {
    return this._http.put<CancelCita>(`${environment.url}cancel`, id, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: id
    }).pipe(map((response: HttpResponse<CancelCita>) => response.body));
  }

  public addProducts(params): Observable<Message> {
    return this._http.post<Message>(`${environment.url}product`, params, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }

  public getCotizaciones(fecha): Observable<Cotizaciones[]> {
    return this._http.get<Cotizaciones[]>(`${environment.url}cotizacion`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {'fecha': fecha}
    }).pipe(map((response: HttpResponse<Cotizaciones[]>) => response.body));
  }

  public aprobar(data): Observable<Message> {
    return this._http.put<Message>(`${environment.url}aprobar`, {}, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: data
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }

  public productos(): Observable<CotizacionesInstalador[]> {
    return this._http.get<CotizacionesInstalador[]>(`${environment.url}nofab`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<CotizacionesInstalador[]>) => response.body));
  }

  public terminar(productos): Observable<Message> {
    return this._http.post<Message>(`${environment.url}terminar`, productos, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }
}
