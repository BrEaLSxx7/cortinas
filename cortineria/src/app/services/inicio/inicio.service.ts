import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Cliente} from '../../interfaces/inicio/cliente';
import {map} from 'rxjs/operators';
import {Hora} from '../../interfaces/inicio/hora';
import {PeticionClientes} from '../../interfaces/inicio/peticion-clientes';
import {PeticionCitas} from '../../interfaces/inicio/peticion-citas';
import {Login} from '../../interfaces/inicio/login';
import {Consultas} from '../../interfaces/inicio/consultas';

@Injectable({
  providedIn: 'root'
})
export class InicioService {

  constructor(private _http: HttpClient) {
  }

  public login(values): Observable<Login> {
    return this._http.post<Login>(`${environment.url}auth`, {'usuario': btoa(values.usuario), contrasena: btoa(values.contrasena)}, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Login>) => response.body));
  }

  public searchCC(cc): Observable<Cliente> {
    return this._http.get<Cliente>(`${environment.url}search`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {'cc': cc.cedula}
    }).pipe(map((response: HttpResponse<Cliente>) => response.body));
  }

  public searchhour(fecha): Observable<Hora[]> {
    return this._http.get<Hora[]>(`${environment.url}hour`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {'fecha': fecha}
    }).pipe(map((response: HttpResponse<Hora[]>) => response.body));
  }

  public newClient(cliente): Observable<PeticionClientes> {
    return this._http.post<PeticionClientes>(`${environment.url}client`, cliente, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<PeticionClientes>) => response.body));
  }

  public newCita(cita): Observable<PeticionCitas> {
    return this._http.post<PeticionCitas>(`${environment.url}cita`, cita, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<PeticionCitas>) => response.body));
  }

  public updateClient(cliente, id): Observable<PeticionClientes> {
    cliente.id = id;
    return this._http.put<PeticionClientes>(`${environment.url}client/${id}`, cliente, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<PeticionClientes>) => response.body));
  }

  public consultar(cc): Observable<Consultas> {
    return this._http.get<Consultas>(`${environment.url}consultas`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {cc}
    }).pipe(map((response: HttpResponse<Consultas>) => response.body));
  }

  public getCiudades(): Observable<any[]> {
    const optios = {
      '$limit': '5000',
      '$$app_token': 'ScI3WGqCiv0p6Qrg0taocrncj'
    };
    return this._http.get<any[]>('https://www.datos.gov.co/resource/p95u-vi7k.json', {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: optios
    }).pipe(map((response: HttpResponse<any[]>) => response.body));
  }
}
