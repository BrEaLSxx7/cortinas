import {Rol} from './rol';

export interface Trabajador {
  usuario: string;
  nombre: string;
  rol_trabajador: Rol;
}
