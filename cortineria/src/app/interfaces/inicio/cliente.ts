export interface Cliente {
  id: number;
  nombre_completo: string;
  direccion: string;
  barrio: string;
  ciudad: string;
  telefono: number;
  cedula: number;
}
