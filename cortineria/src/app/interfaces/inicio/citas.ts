export interface Citas {
  id: number;
  fecha: string;
  aprobado: boolean;
  id_cliente: number;
  id_hora: number;
  tipo_trabajo: string;
}
