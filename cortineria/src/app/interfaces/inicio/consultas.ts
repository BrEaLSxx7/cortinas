export interface Consultas {
  barrio: string;
  cedula: number;
  citas: CitasConsulta[];
  cotizaciones: CotizacionesConsulta[];
  direccion: string;
  id: number;
  ciudad: string;
  nombre_completo: string;
  telefono: string;
}

export interface CitasConsulta {
  id: number;
  fecha: string;
  hora: string;
  tipo_trabajo: string;
  aprobado: any;
}

export interface CotizacionesConsulta {
  descripcion: string;
  terminado: any;
  aprobado: any;
  forma: string;
  id: number;
}
