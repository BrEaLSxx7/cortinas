export interface LineChartOptions {
  responsive: boolean;
  legend: Legend;
}

interface Legend {
  position: string;
}
