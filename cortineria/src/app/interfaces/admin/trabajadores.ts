export interface Trabajadores {
  id: number;
  nombre: string;
  password: string;
  rol_trabajador: RolTrabajador;
  usuario: string;
}

interface RolTrabajador {
  id: number;
  nombre: string;
  descripcion: string;
}
