export interface Citas {
  fecha: string;
  id: number;
  cliente_cita: ClienteCita;
  tipo_trabajo: string;
  hora_cita: HoraCita;
}

interface ClienteCita {
  id: number;
  nombre_completo: string;
  direccion: string;
  ciudad: string;
  cedula: number;
  barrio: string;
  telefono: number;
}

interface HoraCita {
  id: number;
  hora: string;
}
