import {Component, OnInit} from '@angular/core';
import {LineChartData} from '../interfaces/admin/line-chart-data';
import {LineChartOptions} from '../interfaces/admin/line-labels';
import {LineChartColor} from '../interfaces/admin/line-chart-color';
import {TypeChart} from '../interfaces/admin/type-chart';
import {AdminServiceService} from '../services/admin/admin-service.service';
import {ToastrService} from 'ngx-toastr';
import {GetData} from '../interfaces/admin/get-data';
import {SocketService} from '../services/notifications/socket.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public lineChartData: LineChartData[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Citas', fill: false},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Cotizaciones', fill: false},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Aprobadas', fill: false},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Rechazadas', fill: false},
  ];
  public totalCitas = 0;
  public totalCotizaciones = 0;
  public totalAprobadas = 0;
  public totalNoAprobadas = 0;
  private ano = 2018;
  public selectAno = this.ano;
  private anosOn = new Date().getFullYear() - this.ano + 1;
  public anos: number[] = [];
  public lineChartLabels: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  public lineChartOptions: LineChartOptions = {
    responsive: true,
    legend: {
      position: 'top'
    }
  };
  public lineChartColors: LineChartColor[] = [
    {
      backgroundColor: 'rgba(12,85,147,0.2)',
      borderColor: 'rgba(12,85,147,1)',
      pointBackgroundColor: 'rgba(12,85,147,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(12,85,147,0.8)'
    },
    {
      backgroundColor: 'rgba(255,244,53,0.2)',
      borderColor: 'rgba(255,244,53,1)',
      pointBackgroundColor: 'rgba(255,244,53,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,244,53,0.8)'
    },
    {
      backgroundColor: 'rgba(166,226,46,0.2)',
      borderColor: 'rgba(166,226,46,1)',
      pointBackgroundColor: 'rgba(166,226,46,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(166,226,46,0.8)'
    },
    {
      backgroundColor: 'rgba(255,0,0,0.2)',
      borderColor: 'rgba(255,0,0,1)',
      pointBackgroundColor: 'rgba(255,0,0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,0,0,0.8)'
    }
  ];
  public lineChartLegend = true;
  public typeChart: TypeChart[] = [
    {value: 'line', option: 'Lineas'},
    {value: 'bar', option: 'Barras'},
    {value: 'radar', option: 'Radar'},
    {value: 'pie', option: 'Pastel'},
    {value: 'polarArea', option: 'Area'}
  ];
  public lineChartType = this.typeChart[1].value;
  public user: number;
  public spinner = true;

  constructor(private _adminService: AdminServiceService, private _toastr: ToastrService, private socket: SocketService) {
    for (let i = 0; i < this.anosOn; i++) {
      this.anos.push(this.ano++);
    }
  }

  public getData() {
    this.spinner = true;
    this._adminService.getData(this.selectAno).subscribe((response: GetData) => {
      this.totalCitas = response.totalCitas;
      this.totalCotizaciones = response.totalCotizaciones;
      this.totalAprobadas = response.totalAprobadas;
      this.totalNoAprobadas = response.totalNoAprobados;
      const _lineChartData: Array<any> = new Array(this.lineChartData.length);
      for (let i = 0; i < this.lineChartData.length; i++) {
        _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
        switch (i) {
          case 0:
            _lineChartData[i].data = response.citas;
            break;
          case 1:
            _lineChartData[i].data = response.cotizaciones;
            break;
          case 2:
            _lineChartData[i].data = response.aprobadas;
            break;
          case 3:
            _lineChartData[i].data = response.noAprobados;
            break;
        }
      }
      this.lineChartData = _lineChartData;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  ngOnInit() {
    this.socket.getcone().subscribe(response => {
      this.user = response;
    });
    this.socket.getdisco().subscribe(response => {
      this.user = response;
    });
    this.socket.getUser().subscribe(response => {
      this.user = response.conexions;
    }, error1 => console.error(error1));
    this.socket.getMessage().subscribe(() => {
      this.getData();
    });
    this.getData();
  }
}
