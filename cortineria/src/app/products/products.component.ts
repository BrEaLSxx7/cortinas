import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CotizanteService} from '../services/cotizante/cotizante.service';
import {ToastrService} from 'ngx-toastr';
import {SocketService} from '../services/notifications/socket.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  public formProducts: FormGroup;
  private cont = 0;

  constructor(private _cotizanteService: CotizanteService, private _toastr: ToastrService, private socket: SocketService) {
    this.formProducts = new FormGroup({
      pertenece_fab: new FormControl(''),
      descripcion: new FormControl('', Validators.required),
      ancho: new FormControl('', Validators.required),
      largo: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      referencia: new FormControl('', Validators.required),
      ubicacion: new FormControl('', Validators.required),
      cantidad: new FormControl('', Validators.required),
      valor_unitario: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.formProducts.controls['pertenece_fab'].setValue('1');
  }


  public add(form) {
    if (this.cont < 1) {
      this._cotizanteService.addCotizacion(JSON.parse(sessionStorage.getItem('forma'))).subscribe(response => {
        sessionStorage.setItem('cotizacion', JSON.stringify(response));
        this.socket.sendMessage('cotizacion');
        form.value.pertenece_fab = form.value.pertenece_fab === '1';
        form.value.id_cotizacion = JSON.parse(sessionStorage.getItem('cotizacion')).id;
        this._cotizanteService.addProducts(
          form.value
        ).subscribe(response2 => {
          this._toastr.success(response2.message);
        }, error1 => {
          console.error(error1);
          this._toastr.error(error1.errror.message);
        });
        form.reset();
        switch (sessionStorage.getItem('tmp')) {
          case 'solicitar':
            break;
          case 'confirmar':
            break;
          default:
            this._toastr.success('No necesitas confirmación de crédito');
        }
      }, error1 => {
        console.error(error1);
        this._toastr.error(error1.error.message);
      });
      this.cont++;
    } else {
      form.value.pertenece_fab = form.value.pertenece_fab === '1';
      (JSON.parse(sessionStorage.getItem('cotizacion')));
      form.value.id_cotizacion = JSON.parse(sessionStorage.getItem('cotizacion')).id;
      this._cotizanteService.addProducts(
        form.value
      ).subscribe(response2 => {
        this._toastr.success(response2.message);
      }, error1 => {
        console.error(error1);
        this._toastr.error(error1.errror.message);
      });
      form.reset();
    }
  }

  ngOnDestroy(): void {
    this.cont = 0;
  }
}
