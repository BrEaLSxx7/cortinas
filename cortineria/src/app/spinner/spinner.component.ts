import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  private msm = ['Realizando petición...', 'Analizando datos...', 'Construyendo interfaz...', 'Carga completa!'];
  public mensaje: string;

  constructor() {
  }

  ngOnInit() {
  }

}
