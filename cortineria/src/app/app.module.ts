import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {InicioComponent} from './inicio/inicio.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {ConfirmComponent} from './confirm/confirm.component';
import {ToastrModule} from 'ngx-toastr';
import {CotizanteComponent} from './cotizante/cotizante.component';
import {ProductsComponent} from './products/products.component';
import {SocketService} from './services/notifications/socket.service';
import {ModistaComponent} from './modista/modista.component';
import {InstaladorComponent} from './instalador/instalador.component';
import {AdminComponent} from './admin/admin.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TrabajadorComponent} from './trabajador/trabajador.component';
import {TrabajadorsComponent} from './trabajadors/trabajadors.component';
import {SpinnerComponent} from './spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ConfirmComponent,
    CotizanteComponent,
    ProductsComponent,
    ModistaComponent,
    InstaladorComponent,
    AdminComponent,
    DashboardComponent,
    TrabajadorComponent,
    TrabajadorsComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    ToastrModule.forRoot()
  ],
  entryComponents: [ConfirmComponent, ProductsComponent, TrabajadorComponent, DashboardComponent, TrabajadorsComponent, SpinnerComponent],
  providers: [SocketService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
