import { TestBed } from '@angular/core/testing';

import { ModistaService } from './modista.service';

describe('ModistaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModistaService = TestBed.get(ModistaService);
    expect(service).toBeTruthy();
  });
});
