import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Message} from '../../interfaces/global/message';
import {Cotizaciones} from '../../interfaces/modista/cotizaciones';

@Injectable({
  providedIn: 'root'
})
export class ModistaService {

  constructor(private _http: HttpClient) {
  }

  public getCotizaciones(): Observable<Cotizaciones[]> {
    return this._http.get<Cotizaciones[]>(`${environment.url}cotizacion`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Cotizaciones[]>) => response.body));
  }

  public terminarProducto(productos): Observable<Message> {
    return this._http.post<Message>(`${environment.url}terminar`, productos, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }
}
