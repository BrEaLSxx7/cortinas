import { TestBed } from '@angular/core/testing';

import { CotizanteService } from './cotizante.service';

describe('CotizanteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CotizanteService = TestBed.get(CotizanteService);
    expect(service).toBeTruthy();
  });
});
