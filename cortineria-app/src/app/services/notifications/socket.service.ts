import * as io from 'socket.io-client';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socket;

  constructor(private _http: HttpClient) {
    this.socket = io(environment.urlSocket);
  }


  public sendMessage(message) {
    this.socket.emit('send-message', message);
  }

  public getUser(): Observable<any> {
    return this._http.get<any>(environment.urlSocket, {
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getMessage(): Observable<any> {
    return Observable.create((observer) => {
      this.socket.on('receive-message', (message) => {
        observer.next(message);
      });
    });
  }

  public getcone(): Observable<any> {
    return Observable.create((observer) => {
      this.socket.on('receive-user', (message) => {
        observer.next(message);
      });
    });
  }

  public getdisco(): Observable<any> {
    return Observable.create((observer) => {
      this.socket.on('disco-user', (message) => {
        observer.next(message);
      });
    });
  }

}
