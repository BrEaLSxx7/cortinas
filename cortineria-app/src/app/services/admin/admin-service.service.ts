import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {GetData} from '../../interfaces/admin/get-data';
import {Trabajadores} from '../../interfaces/admin/trabajadores';
import {Message} from '../../interfaces/global/message';
import {Rol} from '../../interfaces/global/rol';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  constructor(private _http: HttpClient) {
  }

  public getData(ano): Observable<GetData> {
    return this._http.get<GetData>(`${environment.url}data`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {ano}
    }).pipe(map((response: HttpResponse<GetData>) => response.body));
  }

  public getTrabajadors(): Observable<Trabajadores[]> {
    return this._http.get<Trabajadores[]>(`${environment.url}trabajador`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Trabajadores[]>) => response.body));
  }

  public deleteTrabajador(id): Observable<Message> {
    return this._http.delete<Message>(`${environment.url}delete`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response',
      params: {id}
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }

  public getRol(): Observable<Rol[]> {
    return this._http.get<Rol[]>(`${environment.url}rol`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Rol[]>) => response.body));
  }

  public newTrabajador(trabajador): Observable<Message> {
    return this._http.post<Message>(`${environment.url}trabajador`, trabajador, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }

  public updateTrabajador(trabajador): Observable<Message> {
    return this._http.put<Message>(`${environment.url}trabajador/${trabajador.id}`, {}, {
      headers: {'Content-Type': 'application/json'},
      params: trabajador,
      observe: 'response'
    }).pipe(map((response: HttpResponse<Message>) => response.body));
  }
}
