import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatAutocompleteModule,
  MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatDialogModule, MatExpansionModule, MatIconModule,
  MatInputModule, MatMenuModule, MatNativeDateModule,
  MatOptionModule, MatPaginatorModule, MatProgressSpinnerModule, MatRadioModule,
  MatSelectModule, MatSidenavModule,
  MatStepperModule, MatTableModule, MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {ChartsModule} from 'ng2-charts';

const module = [
  CommonModule,
  MatButtonModule,
  MatToolbarModule,
  MatInputModule,
  MatStepperModule,
  MatDialogModule,
  MatOptionModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatMenuModule,
  MatRadioModule,
  MatExpansionModule,
  MatSidenavModule,
  MatTabsModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  ChartsModule,
  MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatProgressSpinnerModule
];

@NgModule({
  declarations: [],
  imports: [module],
  exports: [module]
})
export class MaterialModule {
}
