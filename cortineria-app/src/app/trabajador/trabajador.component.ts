import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {AdminServiceService} from '../services/admin/admin-service.service';
import {Trabajadores} from '../interfaces/admin/trabajadores';
import {Message} from '../interfaces/global/message';
import {ToastrService} from 'ngx-toastr';
import {TrabajadorsComponent} from '../trabajadors/trabajadors.component';
import {SocketService} from '../services/notifications/socket.service';

@Component({
  selector: 'app-trabajador',
  templateUrl: './trabajador.component.html',
  styleUrls: ['./trabajador.component.scss']
})
export class TrabajadorComponent implements OnInit {
  public dataSource;
  public displayedColumns: string[] = ['id', 'nombre', 'rol', 'usuario', 'password', 'editar', 'eliminar'];
  @ViewChild('paginator') paginator: MatPaginator;
  public spinner = true;

  constructor(private _adminService: AdminServiceService, public dialog: MatDialog, private _toastr: ToastrService, private _socket: SocketService) {
  }

  ngOnInit() {
    this.get();
    this._socket.getMessage().subscribe(() => {
      this.get();
    });
  }

  private get() {
    this.spinner = true;
    this._adminService.getTrabajadors().subscribe((response: any) => {
      this.dataSource = new MatTableDataSource<Trabajadores>(response);
      this.dataSource.paginator = this.paginator;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public delete(id) {
    this.spinner = true;
    this._adminService.deleteTrabajador(id).subscribe((response: Message) => {
      this._toastr.success(response.message);
      this.get();
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public openDialog(type: string, element) {
    let dialog;
    element.type = type;
    sessionStorage.setItem('tbjd', JSON.stringify(element));
    dialog = this.dialog.open(TrabajadorsComponent, {
      height: '300px',
      width: '400px'
    });
    dialog.afterClosed().subscribe(() => {
      this.get();
    });
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
