export interface AddCotizacion {
  id: number;
  fecha_instalacion_men: string;
  fecha_instalacion_may: string;
  id_cita: number;
  id_forma: number;
  aprobado: boolean;
}
