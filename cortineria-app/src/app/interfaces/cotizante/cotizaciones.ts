export interface Cotizaciones {
  id: number;
  nombre: string;
  telefono: number;
  ciudad: string;
  fecha: string;
  aprobado: any;
  forma: string;
  disabled: boolean;
}
