export interface Cotizaciones {
  cedula: string;
  id: number;
  fecha: string;
  nombre: string;
  descripcion: string;
  ciudad: string;
  ancho: any;
  largo: any;
  cantidad: number;
  color: string;
  mts: any;
  fecha_men: string;
  fecha_may: string;
  entre: string;
}
