import {Cliente} from './cliente';

export interface PeticionClientes {
  message: string;
  cliente: Cliente;
}
