import {Citas} from './citas';

export interface PeticionCitas {
  message: string;
  cita: Citas;
}
