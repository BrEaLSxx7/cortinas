export interface CotizacionesInstalador {
  cedula: number;
  id: number;
  fecha: string;
  nombre: string;
  ciudad: string;
  descripcion: string;
  ancho: any;
  largo: any;
  cantidad: number;
  color: string;
  mts: any;
}
