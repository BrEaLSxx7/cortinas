export interface LineChartData {
  data: number[];
  label: string;
  fill: boolean;
}
