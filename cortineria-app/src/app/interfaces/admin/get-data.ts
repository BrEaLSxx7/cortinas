export interface GetData {
  aprobadas: number[];
  citas: number[];
  cotizaciones: number[];
  noAprobados: number[];
  totalAprobadas: number;
  totalCitas: number;
  totalCotizaciones: number;
  totalNoAprobados: number;
}
