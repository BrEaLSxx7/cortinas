import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {InicioService} from '../services/inicio/inicio.service';
import {MAT_DATE_LOCALE, MatDialog} from '@angular/material';
import {ConfirmComponent} from '../confirm/confirm.component';
import {Cliente} from '../interfaces/inicio/cliente';
import {ToastrService} from 'ngx-toastr';
import {Hora} from '../interfaces/inicio/hora';
import {PeticionClientes} from '../interfaces/inicio/peticion-clientes';
import {PeticionCitas} from '../interfaces/inicio/peticion-citas';
import {Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Login} from '../interfaces/inicio/login';
import {Consultas} from '../interfaces/inicio/consultas';
import {map, startWith} from 'rxjs/operators';
import {SocketService} from '../services/notifications/socket.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss'],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'es-co'}]
})
export class InicioComponent implements OnInit {
  public mananas: number[] = [];
  public tardes: number[] = [];
  public municipios: string[];
  public formLogin: FormGroup;
  public search = new FormControl();
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public formConsulta: FormGroup;
  public cc: number;
  public cliente: Cliente;
  public crear: boolean;
  public confirm: boolean;
  public fecha;
  public hora = true;
  public horasHabilitadas: Hora[] = [];
  public horaFinal: any;
  public tipo: string;
  public minDate = new Date();
  public nombre: string;
  private subscription: Subscription;
  private subscription2: Subscription;
  public displayedColumns: string[] = ['id', 'tipo', 'hora', 'fecha', 'realizada'];
  public dataSource;
  public displayedColumns2: string[] = ['id', 'descripcion', 'aprobado', 'forma', 'terminado'];
  public dataSource2;
  filteredOptions: Observable<string[]>;
  public visibility = true;
  public type = 'password';
  public spinner = true;

  constructor(private _inicioService: InicioService, public dialog: MatDialog, private toastr: ToastrService,
              private _router: Router, private socket: SocketService) {
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.cliente = {
      id: 0,
      cedula: 0,
      nombre_completo: '',
      telefono: 0,
      ciudad: '',
      direccion: '',
      barrio: ''
    };
    this.firstFormGroup = new FormGroup({
      cedula: new FormControl('', Validators.required),
    });
    this.formConsulta = new FormGroup({
      cedula: new FormControl('', Validators.required)
    });
    this.secondFormGroup = new FormGroup({
      cedula: new FormControl('', Validators.required),
      nombre_completo: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      direccion: new FormControl('', Validators.required),
      ciudad: new FormControl('', Validators.required),
      barrio: new FormControl('', Validators.required)
    });
    this.formLogin = new FormGroup({
      usuario: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required)
    });

  }

  ngOnInit() {
    if (sessionStorage.getItem('qwertylkjh') !== null) {
      this._router.navigate([JSON.parse(sessionStorage.getItem('qwertylkjh')).user.rol_trabajador.nombre]);
    }
    this._inicioService.getCiudades().subscribe((response: []) => {
      this.municipios = response.map((e: any) => e.municipio);
      this.filteredOptions = this.search.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
    }, error1 => {
      console.error(error1);
    });
    this.spinner = false;
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    this.secondFormGroup.controls['ciudad'].setValue(this.search.value);
    return this.municipios.filter(option => option.toLowerCase().includes(filterValue));
  }

  public next(form: FormGroup, ste, celular) {
    this.spinner = true;
    this.cc = form.value.cedula;
    this._inicioService.searchCC(form.value).subscribe(response => {
      this.cliente = response;
      this.crear = false;
      sessionStorage.setItem('cliente', JSON.stringify(response));
      this.secondFormGroup.controls['cedula'].setValue(response.cedula);
      this.secondFormGroup.controls['nombre_completo'].setValue(response.nombre_completo);
      this.secondFormGroup.controls['telefono'].setValue(response.telefono);
      this.secondFormGroup.controls['direccion'].setValue(response.direccion);
      this.secondFormGroup.controls['ciudad'].setValue(response.ciudad);
      this.secondFormGroup.controls['barrio'].setValue(response.barrio);
      this.spinner = false;
    }, () => {
      this.secondFormGroup.controls['cedula'].setValue(this.cc);
      this.crear = true;
      this.openDialog(ste);
      this.spinner = false;
    });
    const el: HTMLElement = this.tmp2(celular);
    el.click();
  }

  private sendMessage(msg) {
    this.socket.sendMessage(msg);
  }

  private tmp2(celular) {
    if (celular === 'true') {
      return document.getElementById('triger2') as HTMLElement;
    } else {
      return document.getElementById('triger') as HTMLElement;
    }
  }

  public destroy(step) {
    this.horasHabilitadas = [];
    this.mananas = [];
    this.tardes = [];
    this.hora = true;
    this.fecha = '';
    this.tipo = '';
    this.horaFinal = '';
    this.cliente = {
      id: 0,
      cedula: 0,
      nombre_completo: '',
      telefono: 0,
      direccion: '',
      ciudad: '',
      barrio: ''
    };
    this.cc = 0;
    this.dataSource = [];
    this.dataSource2 = [];
    this.formConsulta.reset();
    this.nombre = '';
    step.reset();
  }

  public show() {
    this.visibility = !this.visibility;
    if (this.visibility) {
      this.type = 'password';
    } else {
      this.type = 'text';
    }
  }

  private tmp(celular) {
    if (celular === 'true') {
      return document.getElementById('ottriger2') as HTMLElement;
    } else {
      return document.getElementById('ottriger') as HTMLElement;
    }
  }

  public sgte(client: FormGroup, celular) {
    this.spinner = true;
    const el: HTMLElement = this.tmp(celular);
    if (!this.crear) {
      const cliente: Cliente = JSON.parse(sessionStorage.getItem('cliente'));
      let tmp = true;
      tmp = tmp && (client.value.cedula === cliente.cedula);
      tmp = tmp && (client.value.barrio === cliente.barrio);
      tmp = tmp && (client.value.direccion === cliente.direccion);
      tmp = tmp && (client.value.telefono === cliente.telefono);
      tmp = tmp && (client.value.ciudad === cliente.ciudad);
      tmp = tmp && (client.value.nombre_completo === cliente.nombre_completo);
      if (!tmp) {
        this._inicioService.updateClient(client.value, cliente.id).subscribe((response: PeticionClientes) => {
          sessionStorage.setItem('cliente', JSON.stringify(response.cliente));
          this.toastr.success(response.message);
          this.spinner = false;
        }, error1 => {
          console.error(error1);
          this.toastr.error(error1.error.message);
          this.spinner = false;
        });
      }
      el.click();
      this.spinner = false;
    } else if (this.crear) {
      this.subscription2 = this._inicioService.newClient(client.value).subscribe((response: PeticionClientes) => {
          sessionStorage.setItem('cliente', JSON.stringify(response.cliente));
          this.cliente = response.cliente;
          this.toastr.success(response.message);
          el.click();
          this.subscription2.unsubscribe();
          this.spinner = false;
        },
        error => {
          console.error(error);
          this.toastr.error(error.error.message);
          this.spinner = false;
        });
    }
  }

  private openDialog(ste) {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '400px',
      data: {confirm: this.confirm}
    });
    dialogRef.afterClosed().subscribe(() => {
      this.confirm = sessionStorage.getItem('confirm') === 'true';
      if (!this.confirm) {
        this.destroy(ste);
      }
    });
  }

  public login(formLog: FormGroup) {
    this.spinner = true;
    this.subscription = this._inicioService.login(formLog.value).subscribe((response: Login) => {
        sessionStorage.setItem('qwertylkjh', JSON.stringify(response));
        this._router.navigate([response.user.rol_trabajador.nombre]);
        this.toastr.success('Sesión iniciada correctamente');
        this.spinner = false;
      },
      error => {
        console.error(error);
        this.toastr.error(error.error.message);
        this.spinner = false;
      });
  }

  public selectFecha() {
    this.spinner = true;
    this._inicioService.searchhour(new Date(this.fecha).toString().split(' (')[0]).subscribe(response => {
      if (this.secondFormGroup.value.ciudad === 'Cartago') {
        this.horasHabilitadas = response;
        this.hora = false;
      } else {
        const manana = {'hora': 'En la mañana', 'id': 99};
        const tarde = {'hora': 'En la tarde', 'id': 100};
        let man = 0;
        let tar = 0;
        response.forEach(e => {
          if (e.hora.split(' ')[2] === 'AM') {
            this.mananas.push(e.id);
            man++;
          } else if (e.hora.split(' ')[2] === 'PM') {
            if (e.id === 5) {
              this.mananas.push(e.id);
              man++;
            } else {
              this.tardes.push(e.id);
              tar++;
            }
          }
        });
        if (man === 5) {
          this.horasHabilitadas.push(manana);
        }
        if (tar === 4) {
          this.horasHabilitadas.push(tarde);
        }
        if (this.horasHabilitadas.length < 1) {
          this.toastr.error('Por favor selecione otra fecha, ya se encuentra la agenda copada');
          this.hora = true;
        } else {
          this.hora = false;
        }
      }
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this.hora = true;
      this.toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public reserveCita(step) {
    this.spinner = true;
    if (this.horaFinal > 90) {
      switch (this.horaFinal) {
        case 99:
          this.horaFinal = 1;
          break;
        case 100:
          this.horaFinal = 6;
          break;
      }
    }
    const cita = {
      id_cliente: this.cliente.id,
      fecha: new Date(this.fecha).toString().split(' (')[0],
      tipo_trabajo: this.tipo,
      id_hora: this.horaFinal,
      output: this.secondFormGroup.value.ciudad !== 'Cartago'
    };
    this._inicioService.newCita(cita).subscribe((response: PeticionCitas) => {
      this.destroy(step);
      this.toastr.success(response.message);
      this.sendMessage(response.message);
      this.spinner = false;
    }, error1 => {
      this.toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public myFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 0;
  };


  public consultar(form, celular, ste) {
    this.spinner = true;
    this._inicioService.consultar(form.value.cedula).subscribe((response: Consultas) => {
      this.nombre = response.nombre_completo;
      response.citas.forEach(e => {
        switch (e.aprobado) {
          case true:
            e.aprobado = 'Si';
            break;
          case false:
            e.aprobado = 'Si';
            break;
          default:
            e.aprobado = 'No';
            break;
        }
      });
      this.dataSource = response.citas;
      response.cotizaciones.forEach(e => {
        switch (e.aprobado) {
          case true:
            e.aprobado = 'Aprobado';
            break;
          case false:
            e.aprobado = 'No aprobado';
            break;
          default:
            e.aprobado = 'En espera';
            break;
        }

        switch (e.terminado) {
          case true:
            e.terminado = 'Si';
            break;
          case false:
          case false:
            e.terminado = 'No';
            break;
        }
      });
      this.dataSource2 = response.cotizaciones;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this.toastr.error('No se encuentra el numero de cédula');
      ste.reset();
      this.spinner = false;
    });
    const el = this.tmp3(celular);
    el.click();
  }

  private tmp3(celular) {
    if (celular === 'true') {
      return document.getElementById('ottriger3') as HTMLElement;
    } else {
      return document.getElementById('triger3') as HTMLElement;
    }
  }
}
