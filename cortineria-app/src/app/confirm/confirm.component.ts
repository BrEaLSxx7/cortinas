import {Component, Inject, inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface DialogData {
  confirm: boolean;
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  public onNoClick(nn): void {
    this.data.confirm = nn;
    sessionStorage.setItem('confirm', nn);
    const el: HTMLElement = document.getElementById('none') as HTMLElement;
    el.click();
  }
}
