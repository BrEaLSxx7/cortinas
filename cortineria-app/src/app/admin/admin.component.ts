import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public pantalla = true;
  group = {value: 'dashboard'};
  public spinner = true;

  constructor(private _router: Router, private _toastr: ToastrService) {
    if (sessionStorage.getItem('qwertylkjh') === null) {
      this._router.navigate(['']);
    }
    if (JSON.parse(sessionStorage.getItem('qwertylkjh')).user.usuario !== 'admin') {
      this._router.navigate(['']);
    }
  }

  public logout() {
    sessionStorage.clear();
    this._router.navigate(['']);
    this._toastr.success('Sesión cerrada');
  }

  ngOnInit() {
    // @ts-ignore
    window.onresize = this.resize();
    this.spinner = false;
  }

  private resize(): void {
    this.pantalla = screen.width > 1024;
  }
}
