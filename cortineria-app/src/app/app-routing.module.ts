import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {InicioComponent} from './inicio/inicio.component';
import {CotizanteComponent} from './cotizante/cotizante.component';
import {ModistaComponent} from './modista/modista.component';
import {InstaladorComponent} from './instalador/instalador.component';
import {AdminComponent} from './admin/admin.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TrabajadorComponent} from './trabajador/trabajador.component';
// import {SpinnerComponent} from './spinner/spinner.component';

const routes: Routes = [
  {path: '', component: InicioComponent},
  {path: 'cotizante', component: CotizanteComponent},
  // {path: 'spin', component: SpinnerComponent},
  {path: 'modista', component: ModistaComponent},
  {
    path: 'admin', component: AdminComponent, children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'trabajadores', component: TrabajadorComponent},
      {path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
    ]
  },
  {path: 'instalador', component: InstaladorComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
