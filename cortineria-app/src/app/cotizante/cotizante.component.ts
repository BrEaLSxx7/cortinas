import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Citas} from '../interfaces/cotizante/citas';
import {CotizanteService} from '../services/cotizante/cotizante.service';
import {ToastrService} from 'ngx-toastr';
import {ProductsComponent} from '../products/products.component';
import {Cotizaciones} from '../interfaces/cotizante/cotizaciones';
import {SelectionModel} from '@angular/cdk/collections';
import {CotizacionesInstalador} from '../interfaces/instalador/cotizaciones';
import {Message} from '../interfaces/global/message';
import {SocketService} from '../services/notifications/socket.service';

@Component({
  selector: 'app-cotizante',
  templateUrl: './cotizante.component.html',
  styleUrls: ['./cotizante.component.scss']
})

export class CotizanteComponent implements OnInit {
  public user;
  public displayedColumns: string[] = ['id', 'nombre_completo', 'direccion', 'barrio', 'ciudad',
    'telefono', 'tipo_trabajo', 'fecha', 'hora', 'action'];
  public displayedColumns2: string[] = ['id', 'nombre_completo', 'direccion', 'barrio', 'ciudad', 'telefono', 'hora', 'action'];
  public displayedColumns3: string[] = ['id', 'nombre', 'telefono', 'fecha', 'aprobado', 'forma', 'action'];
  public displayedColumns4: string[] = ['id', 'nombre', 'telefono', 'fecha', 'aprobado', 'forma', 'action'];
  public displayedColumns5: string[] = ['id', 'nombre', 'cantidad', 'color', 'descripcion', 'ancho', 'largo', 'mts', 'fecha', 'select'];

  public dataSource;
  public dataSource2;
  public dataSource3;
  public dataSource4;
  public dataSource5;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator3') paginator3: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  @ViewChild('paginator4') paginator4: MatPaginator;
  @ViewChild('paginator5') paginator5: MatPaginator;
  public minDate = new Date();
  public fecha = new Date();
  public fecha2 = new Date();
  public action = false;
  public selection = new SelectionModel(true, []);
  public spinner = true;

  constructor(private _router: Router, private _cotizanteService: CotizanteService,
              private _toastr: ToastrService, public dialog: MatDialog, private socket: SocketService) {
  }


  public _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          if (!this.dataSource.paginator) {
            this.dataSource.paginator = this.paginator;
          }
          if (!this.dataSource2.paginator) {
            this.dataSource2.paginator = this.paginator2;
          }
          break;
        case 1:
          if (!this.dataSource3.paginator) {
            this.dataSource3.paginator = this.paginator3;
          }
          if (!this.dataSource4.paginator) {
            this.dataSource4.paginator = this.paginator4;
          }
      }
    });
  }

  ngOnInit() {
    if (sessionStorage.getItem('qwertylkjh') === null || JSON.parse(sessionStorage.getItem('qwertylkjh')).user.usuario !== 'cotizante') {
      this._router.navigate(['']);
    }
    this.user = JSON.parse(sessionStorage.getItem('qwertylkjh'));
    this.selectFecha();

  }

  public selectFecha2() {
    this.spinner = true;
    this._cotizanteService.getCotizaciones(new Date(this.fecha2).toString().split(' (')[0]).subscribe(response => {
      response.forEach(e => {
        switch (e.aprobado) {
          case true:
            e.aprobado = 'Aprobado';
            e.disabled = true;
            break;
          case false:
            e.aprobado = 'No aprobado';
            e.disabled = true;
            break;
          default:
            e.aprobado = 'En espera';
            e.disabled = false;
            break;
        }
      });
      this.dataSource3 = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource3.paginator = this.paginator3;
      this.dataSource4 = new MatTableDataSource<Cotizaciones>(response);
      this.dataSource4.paginator = this.paginator4;
      this.productos();
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public myFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 0;
  };

  public selectFecha() {
    this.spinner = true;
    const fecha1 = `${this.fecha.getDay()}/${this.fecha.getMonth()}/${this.fecha.getFullYear()}`;
    const fecha2 = `${new Date().getDay()}/${new Date().getMonth()}/${new Date().getFullYear()}`;
    this.action = fecha1 !== fecha2;
    this._cotizanteService.filterFecha(new Date(this.fecha).toString().split(' (')[0]).subscribe(response => {
      this.dataSource = new MatTableDataSource<Citas>(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource2 = new MatTableDataSource<Citas>(response);
      this.dataSource2.paginator = this.paginator2;
      this.selectFecha2();
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public logout() {
    sessionStorage.clear();
    this._router.navigate(['']);
    this._toastr.success('Sesión cerrada');
  }

  private openDialog(tmp, fecha, id) {
    let dialog;
    switch (tmp) {
      case 'solicitar':
        sessionStorage.setItem('forma', JSON.stringify({'forma': 'Credito', 'fecha': fecha, 'id': id}));
        sessionStorage.setItem('tmp', 'solicitar');
        dialog = this.dialog.open(ProductsComponent, {
          width: '600px',
        });
        break;
      case 'confirmacion':
        sessionStorage.setItem('forma', JSON.stringify({'forma': 'Credito', 'fecha': fecha, 'id': id}));
        sessionStorage.setItem('tmp', 'confirmar');
        dialog = this.dialog.open(ProductsComponent, {
          width: '600px',
        });
        break;
      case 'contado':
        sessionStorage.setItem('forma', JSON.stringify({'forma': 'Contado', 'fecha': fecha, 'id': id}));
        dialog = this.dialog.open(ProductsComponent, {
          width: '600px',
        });
        break;
      case 'aprobado':
        this._cotizanteService.cancelCita({id, fecha}).subscribe(() => {
          this.sendMessage('cancel');
          this.selectFecha();
        }, error1 => {
          console.error(error1);
          this._toastr.error(error1.errror.message);
        });
        break;
    }
    dialog.afterClosed().subscribe(() => {
      this.selectFecha();
    });
  }

  public applyFilter(filterValue: string) {
    this.dataSource3.filter = filterValue.trim().toLowerCase();
  }

  public applyFilter2(filterValue: string) {
    this.dataSource4.filter = filterValue.trim().toLowerCase();
  }

  public applyFilter5(filterValue: string) {
    this.dataSource5.filter = filterValue.trim().toLowerCase();
  }

  public sendMessage(msg) {
    this.socket.sendMessage(msg);
  }

  public productos() {
    this.spinner = true;
    this._cotizanteService.productos().subscribe((response: CotizacionesInstalador[]) => {
      this.dataSource5 = new MatTableDataSource<CotizacionesInstalador>(response);
      this.dataSource5.paginator = this.paginator5;
      this.spinner = false;
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
      this.spinner = false;
    });
  }

  public terminar() {
    if (this.selection.selected.length > 0) {
      this._cotizanteService.terminar(this.selection.selected).subscribe((response: Message) => {
        this._toastr.success(response.message);
        this.productos();
      }, error1 => {
        console.error(error1);
        this._toastr.error(error1.error.message);
      });
    } else {
      this._toastr.error('Por favor selecione un producto a terminar');
    }
  }

  public aprobado(id, aprobado) {
    this._cotizanteService.aprobar({id, aprobado}).subscribe(response => {
      this.selectFecha2();
      this.sendMessage(response.message);
      this._toastr.success(response.message);
    }, error1 => {
      console.error(error1);
      this._toastr.error(error1.error.message);
    });
  }
}
