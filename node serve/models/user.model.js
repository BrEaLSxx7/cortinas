let mongoose = require('mongoose')
let Schema = mongoose.Schema

let userSchemma = new Schema({
    id: {
        type: String,
        required: true,
        max: 100
    },
    token: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('user', userSchemma);