let express = require('express')
// let bodyParser = require('body-parser')
let app = express();
// let user = require('./routes/user.route')
// let mongoose = require('mongoose')

let http = require('http');
let server = http.Server(app);

let socketIO = require('socket.io');
let io = socketIO(server);

const port = process.env.PORT || 3000;

let cont = 0;

io.on('connection', (socket) => {
    console.log('user connected');
    io.emit('receive-user', Object.keys(io.sockets.connected).length)
    socket.on('disconnect', () => {
        console.log('user disconnect')
        io.emit('disco-user', Object.keys(io.sockets.connected).length)
    })
    socket.on('send-message', (message) => {
        io.emit('receive-message', message);
        cont = 0;
    });
});

server.listen(port, () => {
    console.log(`started on port: ${port}`);
});
app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});
app.get('/', (req, res) => {
    if (isJson(req.headers)) {
        res.send({
            conexions: Object.keys(io.sockets.connected).length
        });
    } else {
        res.statusCode = 401
        res.send({
            message: 'Por favor autenticarse'
        });
    }
})
setInterval(() => {
    cont++;
})

function isJson(str) {
    return tmp = str.accept.split(',')[0] === 'application/json';
}