let express = require('express')
let router = express.Router()

let user_controller = require('../controllers/user.controller')

router.get('/test', user_controller.test)
router.post('/insert', user_controller.insert)
module.exports = router